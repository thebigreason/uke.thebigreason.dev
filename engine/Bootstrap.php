<?php

    // Declare page attributes
    $page_attrs = array(
        'title' => NULL,
        'class' => NULL,
        'description' => NULL,
        'mode' => NULL
    );


    // Include global dependencies
    include('Helpers.php');


    // Set up display mode
    if (isset($_COOKIE['mode'])) {

        // Set mode from cookie
        $page_attrs['mode'] = $_COOKIE['mode'];
        $mode['name'] = $page_attrs['mode'];

        if ($_COOKIE['mode'] == 'light') {
            $mode['alt_name'] = 'dark';
        } else {
            $mode['alt_name'] = 'light';
        }

    // Set innital display mode
    } else {

        $page_attrs['mode'] = 'dark';
        $mode['name'] = 'dark';
        $mode['alt_name'] = 'light';

        // Set to cookie with 30 day expration
        setcookie('mode', 'dark', time()+60*60*24*30);

    }
