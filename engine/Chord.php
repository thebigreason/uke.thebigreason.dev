<?php

	// GET query string formatted like Cmaj7-0002
	// First part is used to create chord name
	// Second is chord fingering

	// Get the chord and separate the name from position
	$input_chord = explode('-', $_GET['chord']);


	/**
	  * Parse Chord
	  *
	  * Parses the passed chord abbreviation and outputs detailed name
	  *
	  * @param string $chord The chord abbreviation to translate
	  *
	  * @return array $output_chord An array of abbreviated and detailed versions of the chord name
	  *
	  */
	function parseChord ($chord) {

		// Get the base note
		$letter = substr($chord, 0, 1);

		// Declare vars
		$aug = FALSE;
		$dim = FALSE;
		$flat = FALSE;
		$sus = FALSE;
		$num = NULL;
		$flat_5 = NULL;
		$flat_5th = NULL;
		$fret = FALSE;

		// Check for flat
		if (substr($chord, 1, 1) == 'b') {
			$flat = '&#9837;';
		}

		// Check for sharp
		if (substr($chord, 1, 1) == '#') {
			$flat = '&#9839;';
		}


		// Check for flat 5th
		if (strstr($chord, '7b5')) {
			$flat_5 = '/&#9837;5';
			$flat_5th = '/&#9837;5th';
		}

		// Split chord elements
		$chord_elements = str_split($chord);

		// Loop through elements and extract numbers
		$i = 0;
		foreach ($chord_elements as $elm) {
			if (is_numeric($elm)) {

				// End loop if flat 5th chord
				if ($elm == 5 && $i > 0) {
					break;

				// Append additional numbers
				} else {
					$num .= $elm;
					$i++;
				}

			}
		}

		// Check for major or minor
		if (strstr($chord, 'maj')) {
			$style['abbr'] = 'maj';
			$style['full'] = 'major';
		} else if (strstr($chord, 'm')) {
			$style['abbr'] = 'm';
			$style['full'] = 'minor';
		}

		// Check for augmented
		if (strstr($chord, 'aug')) {
			$style['abbr'] = 'aug';
			$style['full'] = 'augmented';

		// Check for diminished
		} else if (strstr($chord, 'dim')) {
			$style['abbr'] = 'dim';
			$style['full'] = 'diminished';

		// Check for add
		} else if (strstr($chord, ' ')) {
			$style['abbr'] = '+';
			$style['full'] = 'add';

		// Check for 7th suspended 4th
		} else if (strstr($chord, '7sus4')) {
			$style['abbr'] = '7sus';
			$style['full'] = '7th suspended';

			// Overwrite $num with 4
			$num = 4;

		// Check for suspended
		} else if (strstr($chord, 'sus')) {
			$style['abbr'] = 'sus';
			$style['full'] = 'suspended';
		}

		$output_chord['abbr'] = strtoupper($letter) . $sharp . $flat . $style['abbr'] . $num . $flat_5;
		$output_chord['name'] = strtoupper($letter) . $sharp . $flat . ' ' . $style['full'] . ' ' . $num . 'th' . $flat_5th;
		$output_chord['description'] = strtoupper($letter) . $sharp . $flat . ' ' . strtolower($style['full']) . ' ' . $num . 'th' . $flat_5th;

		// Return output
		return $output_chord;
	}

	/**
	  * Parse Position
	  *
	  * Parses the fingering positions and outputs proper makers and positions
	  *
	  * @param string $pos The string of position numbers to translate
	  *
	  * @return array $output_position An array of coordinates and position market names to use.
	  *
	  */
	function parsePosition($pos) {

		// Declare bar chord vars
		$bar = FALSE;
		$bar_pos = FALSE;

		// Convert position string into array
		$position = str_split($pos);

		// Count position entries
		$pos_count = count($position);


		// Set up fret positions
		$frets = array(
			0 => '40',
			1 => '142',
			2 => '259',
			3 => '376',
			4 => '493',
			5 => '611',
			'x' => '40'
		);

		// Set up marker symbols
		$markers = array(
			'x' => 'mute',
			'0' => 'circle',
			'1' => 'disk',
			'2' => 'disk',
			'3' => 'disk',
			'4' => 'disk',
			'5' => 'disk',
			'b2' => 'bar-2',
			'b3' => 'bar-3',
			'b4' => 'bar-4'
		);

		// Loop through position string and translate fingerings to grid
		$i = 4;
		foreach ($position as $key => $val) {

			// Set string position
			$output_position['string_' . $i]['position'] = $frets[$val];

			// Set marker
			$output_position['string_' . $i]['marker'] = $markers[$val];

			// Decrement position number
			$i--;
		}

		if (strstr($pos, ',')) {

			$ex_bar = explode(',', $pos);
			$ex_pos = explode('.', $ex_bar[1]);
			$ex_str = ltrim($ex_pos[0],'b');

			$bar = $ex_pos[0];
			$bar_pos = $ex_pos[1];

			// Set string position
			$output_position['string_' . $ex_str]['position'] = $frets[$bar_pos];

			// Set marker
			$output_position['string_' . $ex_str]['marker'] = $markers[$bar];


		}

		// Return output
		return $output_position;
	}


	// Check for fret variable for grid position
	if (isset($_GET['fret'])) {

		// Don't render nut
		$nut = array(
			'coords' => 'M100,100 L400,100',
			'stroke' => '3'
		);

		// Set fret number position marker
		$fret = $_GET['fret'];

	} else {

		// Render nut
		$nut = array(
			'coords' => 'M100,107.5 L400,107.5',
			'stroke' => '15'
		);

	}

	// Parse chord name
	$chord = parseChord($input_chord[0]);

	// Parse chord position markers
	$position = parsePosition($input_chord[1]);
