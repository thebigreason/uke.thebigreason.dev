<?php

    /**
      * Get resources via cURL
      *
      * @param string $url URI to get
      * @param array $auth Authentication params
      *
      * @return string $data JSON string
      *
      */
    function getCURL ($url, $auth = FALSE) {

        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Send key/secret
        if (isset($auth['key']) && isset($auth['secret'])) {
            curl_setopt($ch, CURLOPT_USERPWD, "{$auth['key']}:{$auth['secret']}");
        }

        if (isset($auth['token'])) {
            curl_setopt($ch, CURLOPT_USERPWD, "{$auth['token']}");
        }

        // grab URL and pass it to the browser
        $data = curl_exec($ch);

        // close cURL resource, and free up system resources
        curl_close($ch);

        // Return results
        return $data;

    }
