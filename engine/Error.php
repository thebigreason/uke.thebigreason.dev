<?php

    // Set page vars
    $page_attrs['title'] = "Error {$_GET['error']}";
    $page_attrs['class'] = "error err_{$_GET['error']}";
    $page_attrs['description'] = "Error {$_GET['error']}";
