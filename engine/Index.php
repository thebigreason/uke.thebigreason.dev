<?php

    // Scan song directory
    $dir = 'songs/';
    $scan = array_diff(scandir($dir), array('..', '.', '.DS_Store'));

    // Parse song-index.json
    $json = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/../engine/song-index.json');
	$song_index = json_decode($json, true);

    // Compare song count with cache and refresh if difference
    //if (count($scan) != count($song_index['songs'])) {

        $find = array(
            'Title: ',
            'Artist: ',
            'Key: ',
            '[',
            ']',
            '"',
            "\r",
            "\n"
        );

        // Start rewrite of json song index
        $data = "{\r\n\t";
        $data .= '"songs" : [' . "\r\n";

        // Loop through files
        $i = 0;
        foreach ($scan as $item) {

            // Open new song object
            if ($i == 0) {
                $data .= "\t\t{";
            } else {
                $data .= ",\r\n\t\t{";
            }

            // Open song file
            $song = file("songs/$item", FILE_SKIP_EMPTY_LINES);

            // Extract title, artist and key and append to song object
            foreach ($song as $key => $line) {
                if (strstr($line, 'Title:')) {
                    $data .= '"title" : "' . str_replace($find, '', $line) . '", ';
                } else if (strstr($line, 'Artist:')) {
                    $data .= '"artist" : "' . str_replace($find, '', $line) .'", ';
                } else if (strstr($line, 'Key:')) {
                    $data .= '"key" : "' . str_replace($find, '', $line) . '", ';
                }
            }

            // Append file name
            $data .= '"file":"' . $item . '", ';

            // Append slug
            $data .= '"slug":"' . str_replace(array('.txt', ' '),array('','-'),$item) . '", ';

            // Append last updated
            $data .= '"last_updated":"' . filemtime("songs/$item") . '"';

            // Close song object
            $data .= "}";

            $i++;
        }

        // Close songs object
        $data .= "\r\n\t]\r\n}";

        // Write data to song index file
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/../engine/song-index.json', $data);

    //}

    $last_alpha = NULL;

    // Loop through songs an build alphabetical division of array
    foreach ($song_index['songs'] as $song) {

        if ($last_alpha != $song['slug'][0]) {
            $alpha_index = 'i-' . $song['slug'][0];
        }

        if (is_numeric($song['slug'][0])) {
            $alpha_index = 'i-#';
        }

        $songs[$alpha_index][] = $song;

    }

    // Capture recent songs
    $recent = $song_index['songs'];

    // Loop through $recent index
    foreach ($recent as $key => $val) {
        $timestamps[$key] = $val['last_updated'];
    }

    // Sort recent array
    array_multisort($timestamps, SORT_DESC, $recent);

    // Capture 10 recent songs
    $recent_songs = array_slice($recent, 0, 10);

    // Set page vars
    $page_attrs['title'] = 'thebigreason Ukulele Charts';
    $page_attrs['class'] = 'home';
    $page_attrs['description'] = 'An online song chart parser for OnSong formatted text documents.';
