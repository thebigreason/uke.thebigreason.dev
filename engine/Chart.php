<?php

	// Load chord position configs
	$json = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/../engine/chord-positions.json');
	$diagrams = json_decode($json, true);

	//die(var_dump($diagrams));

    if (!empty($_GET['song'])) {

		// Declare song array
        $song = array();
		$song['chart'] = NULL;

        // Parse file name
        $uri = str_replace('-',' ',$_GET['song']) . '.txt';

        // Open file
        if (!$file = file("songs/$uri", FILE_SKIP_EMPTY_LINES)) {

			header("Location: /error/404/");
			exit();

		}

		$song['last_updated'] = filemtime("songs/$uri");

        // Prepare format
	    $search = array(
			'| [',
			'] |',
			'/ [',
			'] /',
			']  [',
            '[',
            ']',
            '(',
            ')',

            'Intro:',
            'Chorus:',
            'Chorus 1:',
            'Chorus 2:',
            'Chorus 3:',
            'Chorus 4:',
            'Chorus 5:',
            'Verse:',
            'Verse 1:',
            'Verse 2:',
            'Verse 3:',
            'Verse 4:',
            'Verse 5:',
            'Solo:',
            'Bridge:',
            'Break:',
            'Outro:',
			'Tag:',
			'Refrain:',
			'Instrumental:',
            "\t",
            "\r"
        );
        $replace = array(
			'| <span class="chord">',
			'</span> |',
			'/ <span class="chord">',
			'</span> /',
			'</span> <span class="chord">',
            '<sup class="chord">',
            '</sup>',
            '<em>(',
            ')</em>',
            '<h3>Intro:</h3>',
            '<h3>Chorus:</h3>',
            '<h3>Chorus 1:</h3>',
            '<h3>Chorus 2:</h3>',
            '<h3>Chorus 3:</h3>',
            '<h3>Chorus 4:</h3>',
            '<h3>Chorus 5:</h3>',
            '<h3>Verse:</h3>',
            '<h3>Verse 1:</h3>',
            '<h3>Verse 2:</h3>',
            '<h3>Verse 3:</h3>',
            '<h3>Verse 4:</h3>',
            '<h3>Verse 5:</h3>',
            '<h3>Solo:</h3>',
            '<h3>Bridge:</h3>',
            '<h3>Break:</h3>',
            '<h3>Outro:</h3>',
			'<h3>Tag:</h3>',
			'<h3>Refrain:</h3>',
			'<h3>Instrumental:</h3>',
            '',
            ''
        );

        // Format song text
        foreach ($file as $key => $line) {
            if (strstr($line, 'Title:')) {
                $song['title'] = str_replace('Title: ', '', $line);
            } else if ($key == 0) {
                $song['title'] = $line;
            } else if (strstr($line, 'Artist:')) {
                $song['artist'] = str_replace('Artist:', 'by', $line);
            } else if ($key == 1) {
                $song['artist'] = 'by ' . $line;
            } else if (strstr($line, 'Key:')) {
                $song['key'] = str_replace(array('[',']'),array('<sup class="chord">','</sup>'),$line);
            } else if (strstr($line, 'Time:')) {
                $song['time'] = $line;
            } else if (strstr($line, 'Copyright:')) {
                $song['copyright'] = str_replace('Copyright: ', 'Copyright &copy; ',$line);
            } else if (strstr($line, 'Author:')) {
                $song['author'] = str_replace('Author: ', 'Chart by ',$line);
            } else {
                $song['chart'] .= $line;
            }
        }

		// Set page vars
		$page_attrs['title'] = "{$song['title']}";
		$page_attrs['class'] = 'chart';
		$page_attrs['description'] = "{$song['title']} {$song['artist']}";

        // Parse chart for chords
        preg_match_all("/\[([^\]]*)\]/", $song['chart'], $get_chords);

        // Remove duplicated chords
        $chords = array_unique($get_chords[1]);

		// Pin chords to the side of screen if there are less than 11
		if (count($chords) < 11) {
			$page_attrs['class'] .= ' pinned';
		}

		// Mark up the body of the chart
        $chart = str_replace($search, $replace, $song['chart']);

		// Turn off display for chord diagrams
        if (isset($_GET['no_diagrams']) && $_GET['no_diagrams'] == 1) {
        	$page_attrs['class'] .= ' no-diagrams';
			$diagram_toggle['link'] = '/chart/' . $_GET['song'] . '/';
			$diagram_toggle['name'] = 'Enable Diagrams';
        } else {
			$diagram_toggle['link'] = '/chart/' . $_GET['song'] . '/x/';
			$diagram_toggle['name'] = 'Disable Diagrams';
		}

    } else {

		header("HTTP/1.0 404 Not Found");
		exit();

	}
