<?php

    /**
      * Toggle Display Mode
      *
      * @param string $set_mode Mode to set display to
      *
      * @return array $mode New mode settings
      *
      */
    function toggleDisplayMode ($set_mode) {

        // Declare mode array
        $mode = array();

        // Set new mode and alternat emode
        switch ($set_mode) {
            case 'light':
                $mode['name'] = 'light';
                $mode['alt_name'] = 'dark';
                break;

            default:
                $mode['name'] = 'dark';
                $mode['alt_name'] = 'light';
                break;
        }

        // Set page attr
        $page_attrs['mode'] = $mode['name'];

        // Set cookie with 30 day expiration
        setcookie('mode', $mode['name'], time()+60*60*24*30);

        // Return results
        return $mode;

    }

    // Get referer
    $referer = $_SERVER['HTTP_REFERER'];

    // Check for mode change
    if (isset($_GET['mode'])) {

        // Toggle the display mode
        $mode = toggleDisplayMode($_GET['mode']);

    }

    header("Location: $referer");
    exit();
