<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js <?=$page_attrs['mode']?>"> <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title><?=$page_attrs['title']?></title>
        <meta name="description" content="<?=$page_attrs['description']?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="/css/normalize.min.css" />
        <link rel="stylesheet" href="/css/master.css" />
        <link rel="stylesheet" href="/css/print.css" media="print" />
        <script src="/js/modernizr.custom.67477.min.js"></script>
    </head>
    <body class="<?=$page_attrs['class']?>">
        <nav>
            <ul>
                <li><a href="/">Index</a></li>
                <? if (isset($chords)) { ?>
                    <li><a href="<?=$diagram_toggle['link']?>"><?=$diagram_toggle['name']?></a></li>
                <? } ?>
                <!-- <li><a href="/mode/<?=$mode['alt_name']?>/" id="style"><?=$mode['alt_name']?> mode</a></li> -->
                <!-- <li><?=$_COOKIE['mode']?></li> -->
            </ul>
        </nav>
