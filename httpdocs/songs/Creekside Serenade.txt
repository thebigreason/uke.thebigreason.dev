Title: Creekside Serenade
Artist: Mark Eagleton
Key: [Gm]
Time: 3/4

Verse 1:
| [Gm] / /  | [Gm] / / | [D7] / / | [Gm] / / |
| [Gm] / /  | [Gm] / / | [D7] / / | [D7] / / |
| [Gm] / /  | [Gm] / / | [D7] / / | [Gm] / / |
| [Gm] / /  | [D7] / / | [Gm] / / | [Gm] / / |

Verse 2:
| [Cm] / / | [Cm] / / | [Gm] / / | [Gm] / / |
| [Cm] / / | [Cm] / / | [C7] / / | [D7] / / | [D7] / / |
| [Gm] / / | [Gm] / / | [D7] / / | [Gm] / / |
| [Gm] / / | [D7] / / | [Gm] / / | [Gm] / / |
