Title: The Body Electric
Artist: Hurray for the Riff Raff

[A][Dm][D][Dm]

Said you're gonna [A]shoot me down, put my body in the river
[A]Shoot me down, put my body in the river
[D]While the whole world sings, [Dm]sing it like a song
[D]The whole world sings like there's [Dm]nothing going wrong

[A]He shot her down, he put her body in the river
[A]He covered her up but I went to get her
[D]And I said, "My girl, [Dm]what happened to you now?"
[D]I said, "My girl, [Dm]we gotta stop it somehow"

[A]Ooooooohhh... x2
[D][Dm]ooooohh... 

Oh, [A]and tell me what's a man with a rifle in his hand
Gon[D]na do for a world that's [Dm]so sick and sad?
[A]Tell me what's a man with [Dm]a rifle in his hand
Go[D]nna do for a world that's [Dm]so gone mad?

[A]He's gonna shoot me down, put my body in the river
Cover me up with the leaves of September
[D]Like an old sad song, [Dm]you heard it all before
[D]Well, Delia's gone but [Dm]I'm settling the score
 
[A]Ooooooohhh... x2
[D][Dm]ooooohh... 

[A]Oh, and tell me what's a man with a [Dm]rifle in his hand
Go[D]nna do for a world that's just [Dm]dying slow?
[A]Tell me what's a man with a [Dm]rifle in his hand
[D]Gonna do for his daughter when it's [Dm]her turn to go?