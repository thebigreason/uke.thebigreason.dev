Title: In The Pines
Artist: Alan Riggs
Author: Mark Eagleton
Key: [G]
Time: 3/4

Chorus:
In the [G]pines in the pines
Where the [C]sun never [G]shines
And you shiver when the [D7]cold wind [G]blows

Verse:
[G]My love my love what [C]have I [G]done
To make you [D7]treat me [G]so
You've [G]caused me to weep
You've [C]caused me [G]to moan
You've caused me to [D7]leave my [G]home

(Chorus)

Verse:
[G]The longest train I [C]ever [G]saw
Went down that ole [D7]Georgia [G]line
The engine passed at [C]6 o'c[G]lock
The caboose went [D7]by at [G]nine

(Chorus)

Verse:
[G]I asked my captain
For the [C]time of [G]day
He said he throwed his [D7]watch [G]away

(Chorus)
