Title: Sittin On Top of the World
Artist: Doc Watson
Author: Mark Eagleton
Key: [D]
Time: 4/4

It was in the [D]spring one sunny [D7]day
My good gal [G]left me Lord she went a[D]way
And now she's [D]gone but I don't [A]worry
'Cause I'm [D]sittin' on [G]top of the [D]world

She called me [D]up from down in El pa[D7]so
Said come back, [G]daddy, Lord I need you [D]so
And now she's [D]gone but I don't [A]worry
'Cause I'm [D]sittin' on [G]top of the [D]world

Ashes to [D]ashes, dust to [D7]dust
Show me a [G]woman a man can [D]trust
And now she's [D]gone but I don't [A]worry
'Cause I'm [D]sittin' on [G]top of the [D]world

Mississippi [D]River, long, deep and [D7]wide
The woman I'm [G]loving is on the other [D]side
And now she's [D]gone but I don't [A]worry
'Cause I'm [D]sittin' on [G]top of the [D]world

You don't like my [D]peaches, don't you shake my [D7]tree
Get out of my [G]orchard, let my peaches [D]be
And now she's [D]gone but I don't [A]worry
'Cause I'm [D]sittin' on [G]top of the [D]world

Don't you come here [D]running, holding out your [D7]hand
I'll get me a [G]woman like you got your [D]man
And now she's [D]gone but I don't [A]worry
'Cause I'm [D]sittin' on [G]top of the [D]world
