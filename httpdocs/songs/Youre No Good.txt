Title: You're No Good
Artist: Linda Ronstadt
Key: [Am]
Author: Mark Eagleton

Intro:
| [Am] / [D] / | [Am] / [D] / | (2x)

Verse:
[Am]Feeling [D]better, [Am]now that we're [D]through
[Am]Feeling [D]better, [Am]'cause I'm over [D]you
I've [F]learned my [G]lesson, it [C]left a scar
[Am]Now I [D]see how you rea[E]lly are

Chorus:
You're no [Am]good, you're no [D]good, you're no [Am]good
Baby, [D]you're no [Am]good, [D]I'm gonna [Am]say it a[D]gain,
You're no [Am]good, you're no [D]good, you're no [Am]good
Baby, [D]you're no [Am]good [D] [Am] [D]

Verse:
I [Am]broke a [D]heart that's [Am]gentle and [D]true
Well, I [Am]broke a [D]heart over s[Am]omeone like [D]you
I'll [F]beg his for[G]giveness on [C]bended knee
[Am]I wouldn't [D]blame him if he [E]said to me

(Chorus)

Break:
[Am] [D] [F] 2x
[F] [G] [C] [Am] [D] [E]

Bridge:
I'm telling [Am]you now, [D]baby, that I'm [Am]goin' my [D]way
[Am]Forget a[D]bout you, baby, 'cause I'm [Am]leaving this [D]day

(Chorus)

Outro:
[Am] [C] [D] [D](bend)
