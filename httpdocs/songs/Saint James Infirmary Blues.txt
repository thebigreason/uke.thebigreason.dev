Title: St. James Infirmary Blues 
Artist: Traditional
Author: Mark Eagleton
Key: [Gm]

	    Gm 5fr.         D7 2fr.         Gm 1fr.
T 1|-------5-------|-------3-------|-------1-------|
A 2|-------6-------|-------2-------|-------3-------|
B 3|-------7-------|-------2-------|-------2-------|
  4|-------0-------|-------2-------|-------0-------|
  
  	      Cm              Bb7             Eb7
T 1|-------3-------|-------1-------|-------4-------|
A 2|-------3-------|-------1-------|-------3-------|
B 3|-------3-------|-------2-------|-------3-------|
  4|-------5-------|-------1-------|-------3-------|

  	      D7              C7            D7 5fr.
T 1|-------0-------|-------1-------|-------5-------|
A 2|-------2-------|-------0-------|-------5-------|
B 3|-------0-------|-------0-------|-------6-------|
  4|-------2-------|-------0-------|-------5-------|

Verse:
I went [Gm(5fr.)]down to [D7(2fr.)]St. James [Gm(1fr.)]infirmary
To [Gm(5fr.)]see my [Cm]baby [D7(2fr.)]there
She was [Gm(5fr.)]stretched out on a [D7(2fr.)]long white [Gm(1fr.)]table, 
So [Eb7]cold, so [D7]pale, so [Gm(1fr.)]fair.

Verse:
It went [Gm]down to [D7]old Joe's [Gm]barroom
On the [Gm]corner [Cm]by the [D7]square
The [Gm]drinks [D7]served as [Gm]usual, 
And the [Eb7]usual [D7]crowd was [Gm]there.

Verse:
Up [Gm]steps [D7]Joe Mc[Gm]Ginity, 
His [Gm]eyes were [Cm]bloodshot [D7]red,
He [Gm]turned to the [D7]crowd a[Gm]round him
And [Eb7]these were the [D7]words he [Gm]said:

Chorus:
Let her [Gm(5fr.)]go, let her [D7(2fr.)]go, God [Gm(1fr.)]bless her
Wher[Gm(5fr.)]ever [C7]she may [D7(5fr.)]be,
You can [Gm(5fr.)]search this [D7]wide world [Gm(1fr.)]over[Bb7]
And [Eb7]never find another girl as [D7(2fr.)]nice as [Gm(1fr.)]she.

Verse:
I went [Gm]down to [D7]St. James [Gm]infirmary
To [Gm]see my [Cm]baby [D7]there
She was [Gm]stretched out on a [D7]long white [Gm]table, 
So [Eb7]cold, so [D7]pale, so [Gm]fair.

Chorus:
Let her [Gm]go, let her [D7]go, God [Gm]bless her
Wher[Gm]ever [C7]she may [D7]be,
You can [Gm]search this [D7]wide world [Gm]over[Bb7]
And [Eb7]never find another girl as [D7]nice as [Gm]she.

Verse:
Six [Gm]men going [D7]to the [Gm]graveyard,
[Gm]Six in an [Cm]oldtime [D7]hack,
Six [Gm]men going [D7]to the [Gm]graveyard, 
But only [Eb7]five are [D7]coming [Gm]back.

Chorus:
Let her [Gm]go, let her [D7]go, God [Gm]bless her
Wher[Gm]ever [C7]she may [D7]be,
You can [Gm]search this [D7]wide world [Gm]over[Bb7]
And [Eb7]never find another girl as [D7]nice as [Gm]she.
