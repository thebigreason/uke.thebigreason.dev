Title: Margaritaville
Artist: Jimmy Buffett
Key: [D]
Author: Mark Eagleton

Verse:
[D]Nibblin' on sponge cake,
[D]watchin' the sun bake;
All of those tourists covered with [A7]oil.
[A7]Strummin' my six string
[A7]On my front porch swing.
Smell those shrimp
They're beginnin' to [D]boil[D7].

Chorus:
[G]Wasted a[A]way again in Marga[D]ritaville,[D7]
[G]Searchin' for my [A]lost shaker of [D]salt.[D7]
[G]Some people [A]claim that there's a 
[D]Wo[A]man to [G]blame,
But I [A7]know it's nobody's [D]fault.

Verse:
[D]Don't know the reason,
[D]Stayed here all season
With nothing to show but this brand new tat[A7]too.
But it's a real [A7]beauty,
A Mexican [A7]cutie, how it got here
I haven't a [D]clue.[D7]

(Chorus)

Verse:
[D]I blew out my flip flop,
[D]Stepped on a pop top,
Cut my heel, had to cruise on back [A7]home.
But there's [A7]booze in the blender,
And soon it will [A7]render
That frozen concoction that helps me hang [D]on.[D7]

(Chorus)
