Title: Little Brown Jug
Artist: Joseph Winner
Copyright: 1869
Author: Michelle Kiba ukalady.com
Key: [C]
Time: 4/4

My [C]wife and I lived [F]all alone
In a [G7]little log hut we [C]called our own
[C]She loves gin and [F]I love rum
I [G7]tell you we had [C]lots of fun

Chorus:
[C]Ha ha ha [F]you and me
[G7]Little brown jug how [C]I love thee
[C]Ha ha ha [F]you and me
[G7]Little brown jug how [C]I love thee

(Chorus)

'Tis [C]you who makes my [F]friends and foes
'Tis [G7]you who makes me [C]wear old clothes
Here you [C]are so near my [F]nose
So [G7]tip her up and [C]down she goes

(Chorus)

When [C]I go toiling [F]on my farm
[G7]Little brown jug un[C]der my arm
[C]Place him under a [F]shady tree
[G7]Little brown jug don't [C]I love thee

(Chorus)

[C]Crossed the creek on a [F]hollow log
[G7]Me and the wife and our [C]little brown dog
The [C]wife and dog fell [F]in kerplunk
But [G7]I held on to my [C]little brown jug
