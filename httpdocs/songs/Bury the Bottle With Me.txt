Title: Bury the Bottle With Me
Artist: Dick Curless
Key: G

Intro: 
[G] [Em] [G] [Em]
   
There's a [G]stone in yonder [Em]graveyard 
With my [G]name carved in it [G7]deep
It don't [C]tell my life [G]story 
These [C]things it can't [G]repeat

I [G]never had a [Em]family 
I [G]never took a [G7]wife
All I [C]had was a [G]bottle 
And I [C]drank away my [G]life

Chorus:
So [Em]bury the bottle with me 
For [G]it's what tore me [G7]down
So [C]I won't be a[G]lone tonight 
When they [C]put me in the [G]ground
When they [C]lower my body [G]down

A [G]drunkard is a [Em]sinner 
On [G]this I place no [G7]doubt
Oh the [C]Lord won't share his [G]palace 
With the [C]things he lives [G]without

For the [G]bottle is the [Em]devil 
And [G]drinking is his [G7]name
Now [C]the bottle is what [G]took my soul 
And [C]petrified my [G]brain

(Chorus)

