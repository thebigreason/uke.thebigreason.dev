<?
    include('../engine/Bootstrap.php');
    include('../engine/Chart.php');
    include('inc/header.inc.php');
?>

<header>
    <? if (isset($song['title'])) { ?>
    <h1><?=$song['title']?></h1>
    <? } ?>
    <? if (isset($song['artist'])) { ?>
    <h2><?=$song['artist']?></h2>
    <? } ?>
    <p class="meta">
        <? if (isset($song['time'])) { echo $song['time']; } ?>
        <? if (isset($song['key'])) { echo $song['key']; } ?>
    </p>
</header>

<pre><?=$chart?></pre>

<? if ($chords) { ?>
<aside class="chords <?=$chord_class?>">
    <h2>Chords in this&nbsp;song</h2>
    <? foreach ($chords as $chord) { ?>
        <? if ($diagrams['chords'][strtolower($chord)]) { ?>
    <img src="/chord.php?chord=<?=strtolower(urlencode($chord))?>-<?=$diagrams['chords'][strtolower($chord)]?>" alt="<?=$chord?>" id="diagram-<?=str_replace('#','-sharp-', $chord)?>" />
        <? } else { ?>
        <div class="no-chord">
            <p>No diagram for <?=$chord?></p>
        </div>
        <? } ?>
    <? } ?>
</aside>
<? } ?>

<footer>
<? if ($song['copyright'] && $song['author']) { ?>
    <p><?=$song['copyright']?> <?=$song['author']?></p>
<? } ?>
    <p><small>Updated <?=date('M j, Y', $song['last_updated'])?></small></p>
</footer>
<? include('inc/footer.inc.php'); ?>
