<?
    include('../engine/Bootstrap.php');
    include('../engine/Index.php');
    include('inc/header.inc.php');
?>
    
<div class="song-indexes">
    
    <? if ($songs) { ?>
    <section class="all-songs">
        <header>
            <h2>All Songs in Alphabetical Order</h2>
        </header>
        <nav>
            <? foreach ($songs as $key => $index) { ?>
                <a href="#<?=$key?>"><?=$key[2]?></a> 
            <? } ?>
            <a href="#recent-updates" class="recent">Recent Updates</a>
        </nav>
        
        <? foreach ($songs as $key => $index) { ?>
            <h3 id="<?=$key?>"><?=$key[2]?></h3>
            <ol class="listing">
                <? foreach ($index as $song) { ?>
                <li>
                    <a href="/chart/<?=$song['slug']?>/" class="title"><?=$song['title']?></a><br />
                    <? if (isset($song['artist'])) { ?>
                        <span class="artist"> <?=$song['artist']?></span>
                    <? } ?>
                    <? if (isset($song['artist']) && isset($song['key'])) { ?> 
                        &ndash; 
                    <? } ?>
                    <? if (isset($song['key'])) { ?>
                        <span class="key"><?=$song['key']?></span>
                    <? } ?>
                    <small class="updated">Updated <span class="last-updated"><?=date('M j, Y', $song['last_updated'])?></span></small>
                </li>
                <? } ?>
            </ol>
        <? } ?>
        
    </section>
    <? } ?>

    <? if ($recent_songs) { ?>
    <section class="recent-songs" id="recent-updates">
        <header>
            <h2>Recent Updates</h2>
        </header>
        <ol class="listing">
        <? foreach ($recent_songs as $song) { ?>
            <li>
                <a href="/chart/<?=$song['slug']?>/" class="title"><?=$song['title']?></a><br />
                <? if (isset($song['artist'])) { ?>
                    <span class="artist"> <?=$song['artist']?></span>
                <? } ?>
                <? if (isset($song['artist']) && isset($song['key'])) { ?> 
                    &ndash; 
                <? } ?>
                <? if (isset($song['key'])) { ?>
                    <span class="key"><?=$song['key']?></span>
                <? } ?>
                <small class="updated">Updated <span class="last-updated"><?=date('M j, Y', $song['last_updated'])?></span></small>
            </li>
        <? } ?>
        </ol>
    </section>
    <? } ?>

</div>

<? include('inc/footer.inc.php'); ?>
