<?php
	include($_SERVER['DOCUMENT_ROOT'] . '/../engine/Chord.php');
	header("Content-type: image/svg+xml");
	echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="500px" height="800px" viewBox="0 0 500 800" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<title>Ukulele <?=$chord['name']?> Chord</title>
	<desc>Chord diagram for <?=$chord['description']?> on the ukulele.</desc>
	<def>
		<rect id="bar-2" width="160" height="60" rx="30" />
		<rect id="bar-3" width="260" height="60" rx="30" fill="#000000" />
		<rect id="bar-4" width="360" height="60" rx="30" fill="#000000" />
		<circle id="circle" cx="30" cy="30" r="28" fill="transparent" stroke="#000000" stroke-width="3" />
		<circle id="disk" cx="30" cy="30" r="30" fill="#000000" />
		<g id="mute">
			<line fill="none" stroke="#000000" stroke-width="3" stroke-miterlimit="58" x1="58" y1="1" x2="1" y2="58" />
			<line fill="none" stroke="#000000" stroke-width="3" stroke-miterlimit="58" x1="58" y1="58" x2="1" y2="1" />
		</g>
        <g id="grid">
            <path d="M100,101.5 L100,700" id="string-4" stroke="#000000" stroke-width="3" stroke-linecap="square"></path>
            <path d="M200,100 L200,700" id="string-3" stroke="#000000" stroke-width="3"></path>
            <path d="M300,100 L300,700" id="string-2" stroke="#000000" stroke-width="3"></path>
            <path d="M400,101.5 L400,700" id="string-1" stroke="#000000" stroke-width="3" stroke-linecap="square"></path>
            <path d="M100,700 L400,700" id="fret-5" stroke="#000000" stroke-width="3"></path>
            <path d="M100,581.5 L400,581.5" id="fret-4" stroke="#000000" stroke-width="3"></path>
            <path d="M100,464.5 L400,464.5" id="fret-3" stroke="#000000" stroke-width="3"></path>
            <path d="M100,347.5 L400,347.5" id="fret-2" stroke="#000000" stroke-width="3"></path>
            <path d="M100,230.5 L400,230.5" id="fret-1" stroke="#000000" stroke-width="3"></path>
            <path d="<?=$nut['coords']?>" id="nut" stroke="#000000" stroke-width="<?=$nut['stroke']?>"></path>
        </g>
	</def>
	<? if ($fret) { ?>
	<text id="chord-name" x="0" y="426" transform="translate(50)" font-family="Helvetica Neue, Heletica, Arial" font-size="64" fill="#000000">
		<tspan text-anchor="middle"><?=$fret?></tspan>
	</text>
	<? } ?>
	<use xlink:href="#<?=$position['string_4']['marker']?>" x="70" y="<?=$position['string_4']['position']?>" />
	<use xlink:href="#<?=$position['string_3']['marker']?>" x="170" y="<?=$position['string_3']['position']?>" />
	<use xlink:href="#<?=$position['string_2']['marker']?>" x="270" y="<?=$position['string_2']['position']?>" />
	<use xlink:href="#<?=$position['string_1']['marker']?>" x="370" y="<?=$position['string_1']['position']?>" />
	<use xlink:href="#grid" x="0" y="0" />
	<text id="chord-name" x="0" y="780" transform="translate(250)" font-family="Helvetica Neue, Heletica, Arial" font-size="64" fill="#000000">
		<tspan text-anchor="middle"><?=$chord['abbr']?></tspan>
	</text>
</svg>
