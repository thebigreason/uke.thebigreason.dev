$(document).ready(function() {

    // Capture page class
    var page = $('body').attr('class');

    // Behavior for chart page
    if (page == 'chart' || page == 'chart pinned') {

        // Handle chord diagram popups
        $('pre').on("click", ".chord", function(){

            // Capture text of element
            var name = $(this).text();

            // Use name of element to find ID of image (replace # with -sharp- because # is invalid ID character)
            var id = '#diagram-' + name.replace('#','-sharp-');

            // Capture the path/query string of the chord diagram image
            var path = $(id).attr('src');

            // Remove all popups
            $('.popup').remove();

            // Append the popup image to the chord button
            $(this).after('<img src="' + path + '" alt="'+ name +'" class="popup" />');

        });

        // Remove chord popups
        $('pre').on("click", ".popup",function(){
            $(this).remove();
        });
    }

});
